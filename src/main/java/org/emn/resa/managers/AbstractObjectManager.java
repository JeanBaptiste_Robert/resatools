package org.emn.resa.managers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.HashMap;
import java.util.Map;
import java.util.HashMap;

public abstract class AbstractObjectManager {
	private static EntityManagerFactory emf;
	protected static EntityManager em;
	
	/**
	 * Create entityManagerFactory, entityManager and launch the transaction
	 */
	protected static void init(){
		
		Map<String, String> persistenceMap = new HashMap<String, String>();

		String mysqlhost = System.getProperty("MYSQL_SERVICE_HOST");
		if (mysqlhost == null) {
			mysqlhost = "localhost";
		}

		String mysqlPort = System.getProperty("MYSQL_SERVICE_PORT");
		if (mysqlPort == null) {
			mysqlPort = "3306";
		}

		String mysqlDBName = System.getProperty("MYSQL_DATABASE");
		if (mysqlDBName == null) {
			mysqlDBName = "resaToolsDB";
		}

		String mysqlUser = System.getProperty("MYSQL_USER");
		if (mysqlUser == null) {
			mysqlUser = "root";
		}

		String mysqlPassword = System.getProperty("MYSQL_USER_PASSWORD");
		if (mysqlPassword == null) {
			mysqlPassword = "root";
		}

		persistenceMap.put("javax.persistence.jdbc.url", "jdbc:mysql://"+mysqlhost+":"+mysqlPort+"/"+mysqlDBName);
		persistenceMap.put("javax.persistence.jdbc.user", mysqlUser);
		persistenceMap.put("javax.persistence.jdbc.password", mysqlPassword);
 	   	persistenceMap.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");

		emf = Persistence.createEntityManagerFactory("resaTools", persistenceMap);
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}
	
	protected static void close(){
		emf.close();
		em.close();
	}
}
