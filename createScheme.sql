CREATE DATABASE `resaToolsDB`;

SHOW DATABASES;
USE resaToolsDB;
CREATE TABLE `USER` (
  `login` varchar(45) DEFAULT NULL,
  `pwd` varchar(45) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `resaToolsDB`.`USER` (`login`, `pwd`, `id`, `firstname`, `name`, `phone`, `mail`, `admin`) 
VALUES ('test', 'test', 1, 'firstname', 'name', '02', 'mail', 1);
